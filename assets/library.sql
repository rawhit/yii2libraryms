-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2018 at 07:15 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) NOT NULL,
  `book_title` varchar(200) DEFAULT NULL,
  `author_name` varchar(100) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `issued_status` enum('issued','available','pending') NOT NULL DEFAULT 'available'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `book_title`, `author_name`, `year`, `issued_status`) VALUES
(1, 'godfather', 'mario puzo', 4, 'issued'),
(2, 'othello', 'shakespeare', 3, 'issued'),
(3, 'harry potter', 'j k rowling', 32, 'issued'),
(4, 'hamlet', 'shakespear', 12, 'issued'),
(5, 'hamlet', 'shakespear', 12, 'issued'),
(6, 'hamlet', 'shakespear jackson', 22, 'issued'),
(7, 'hamlet', 'shakespear jackson', 22, 'issued'),
(8, 'sherlock holmse', 'conan doyle', 200, 'issued'),
(9, 'you can win', 'shiv khera', 1999, 'pending'),
(10, 'java complete reference', 'herbert schmidt', 1998, 'issued'),
(11, 'hello book', 'unknown', 3000, 'issued'),
(12, 'test', 'test', 1231, 'issued'),
(13, 'macbeth', 'shakespeare', 1522, 'pending'),
(14, 'the alchemist', 'paulo coalo', 1997, 'available'),
(15, 'animal farm', 'george orwell', 1947, 'issued');

-- --------------------------------------------------------

--
-- Table structure for table `issue_history`
--

CREATE TABLE `issue_history` (
  `userid` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `status` enum('pending','approved') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issue_history`
--

INSERT INTO `issue_history` (`userid`, `book_id`, `issue_date`, `status`) VALUES
(3, 3, '2018-11-08', 'approved'),
(1, 4, '2018-11-08', 'approved'),
(1, 5, '2018-11-08', 'pending'),
(1, 6, '2018-11-08', 'approved'),
(1, 7, '2018-11-09', 'approved'),
(1, 8, '2018-11-09', 'pending'),
(1, 9, '2018-11-09', 'pending'),
(1, 10, '2018-11-09', 'approved'),
(1, 11, '2018-11-09', 'approved'),
(1, 12, '2018-11-09', 'approved'),
(1, 3, '2018-11-11', 'pending'),
(12, 1, '2018-11-11', 'approved'),
(12, 14, '2018-11-12', 'pending'),
(12, 8, '2018-11-12', 'approved'),
(12, 13, '2018-11-12', 'pending'),
(12, 15, '2018-11-12', 'approved'),
(12, 5, '2018-11-12', 'approved'),
(12, 13, '2018-11-12', 'pending'),
(12, 8, '2018-11-12', 'approved'),
(12, 9, '2018-11-12', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(100) NOT NULL,
  `username` varchar(121) NOT NULL,
  `password` varchar(111) NOT NULL,
  `usertype` enum('admin','employee','librarian') DEFAULT 'employee',
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `creation_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `password`, `usertype`, `name`, `email`, `creation_date`) VALUES
(1, 'rohithr', 'b1aa8190afa860c969f08dcc7359bfd0', 'employee', 'rohith', 'rohithr@gmail.com', '2018-11-07 11:51:16'),
(2, 'rahulr', 'ba33b6b3bb70c6ce58e42e7881a58f40', 'admin', 'rahul', 'rahul@gmail.com', '2018-11-07 11:51:16'),
(3, 'vineethv', 'vineethv', 'employee', 'vineeth', 'vineeth@yahoo.com', '2018-11-07 11:51:16'),
(4, 'arjuns', 'a3d55c11e5a4474a0eff1ff4655317eb', 'librarian', 'arjun', 'arjun@outlook.com', '2018-11-07 11:51:16'),
(6, 'james', 'b4cc344d25a2efe540adbf2678e2304c', 'employee', 'james mattis', 'james@gmail.com', '2018-11-09 17:20:29'),
(7, 'thomas', 'thomas', 'employee', 'tom sawyer', 'tom@gmail.com', '2018-11-09 17:22:12'),
(8, 'alfred', '29cb2448018800ab65a9de297548b6e0', 'admin', 'alfred william', 'alfred@yahoo.com', '2018-11-09 17:24:05'),
(9, 'john', 'john', 'employee', 'john samuel', 'johns@outlook.com', '2018-11-11 19:49:31'),
(10, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'admin', 'admin', 'admin@library.com', '2018-11-11 20:03:03'),
(12, 'employee', '5f4dcc3b5aa765d61d8327deb882cf99', 'employee', 'employee', 'employee@library.com', '2018-11-11 20:05:22'),
(14, 'librarian', '5f4dcc3b5aa765d61d8327deb882cf99', 'employee', 'librarian', 'librarian@library.com', '2018-11-11 20:09:49'),
(15, 'tester', '5f4dcc3b5aa765d61d8327deb882cf99', 'employee', 'tester', 'tester@library.com', '2018-11-11 20:11:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `issue_history`
--
ALTER TABLE `issue_history`
  ADD KEY `userid` (`userid`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `issue_history`
--
ALTER TABLE `issue_history`
  ADD CONSTRAINT `issue_history_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`),
  ADD CONSTRAINT `issue_history_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

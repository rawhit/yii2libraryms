<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $userid
 * @property string $username
 * @property string $password
 * @property string $usertype
 * @property string $name
 * @property string $email
 * @property string $creation_date
 *
 * @property IssueHistory[] $issueHistories
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['usertype'], 'string'],
            [['creation_date'], 'safe'],
            [['username'], 'string', 'max' => 121],
            [['password'], 'string', 'max' => 111],
            [['name', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'username' => 'Username',
            'password' => 'Password',
            'usertype' => 'Usertype',
            'name' => 'Name',
            'email' => 'Email',
            'creation_date' => 'Creation Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueHistories()
    {
        return $this->hasMany(IssueHistory::className(), ['userid' => 'userid']);
    }
}

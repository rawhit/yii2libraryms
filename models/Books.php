<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property int $book_id
 * @property string $book_title
 * @property string $author_name
 * @property int $year
 * @property string $issued_status
 *
 * @property IssueHistory[] $issueHistories
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year'], 'integer'],
            [['issued_status'], 'string'],
            [['book_title'], 'string', 'max' => 200],
            [['author_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'book_id' => 'Book ID',
            'book_title' => 'Book Title',
            'author_name' => 'Author Name',
            'year' => 'Year',
            'issued_status' => 'Issued Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueHistories()
    {
        return $this->hasMany(IssueHistory::className(), ['book_id' => 'book_id']);
    }
}
